﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Practise.core.DapperHelperEntity;
using Practise.core.Db;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practise.Infrastructure
{
    public class DapperHelperRepo
    {

        public bool IsMasterDb { get; set; } = true;
        public DbIdGetResponse? DbResponse { get; set; }

        private readonly IConfiguration _configuration;

        private static string connectionString = string.Empty;

        public DapperHelperRepo(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionString = _configuration["ConnectionStrings:PracticeDemo"]!;

        }


        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(connectionString);
            }
        }

        public T QuerySingle<T>(string spName, object param)
        {
            using (IDbConnection connection = Connection) // Fixing the 'using' statement
            {
                if (IsMasterDb)
                {
                    connection.ConnectionString = string.Format(_configuration.GetConnectionString("PracticeDemo"), DbResponse?.DbName);
                }
                else
                {
                    connection.ConnectionString = string.Format(_configuration.GetConnectionString("Test"), DbResponse?.DbName);
                }

                var result = connection.Query<T>(spName, param, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return result;
            }
        }

        public List<T> QueryMultiple<T>(string spName, object param)
        {
            using (IDbConnection connection = Connection) // Fixing the 'using' statement
            {
                if (IsMasterDb)
                {
                    connection.ConnectionString = string.Format(_configuration.GetConnectionString("PracticeDemo"), DbResponse?.DbName);
                }
                else
                {
                    connection.ConnectionString = string.Format(_configuration.GetConnectionString("Test"), DbResponse?.DbName);
                }

                var result = connection.Query<T>(spName, param, commandType: CommandType.StoredProcedure).ToList();
                return result;
            }
        }




        public DbIdGetResponse GetDbName (int id)
        {
            DbIdGetResponse dbIdGet  = new DbIdGetResponse();
            using IDbConnection db = Connection;
            dbIdGet = db.QueryFirstOrDefault<DbIdGetResponse>("[dbo].[DbNameGet]", new {id} , commandType : CommandType.StoredProcedure);
            return dbIdGet;
 
        }


    }
}
