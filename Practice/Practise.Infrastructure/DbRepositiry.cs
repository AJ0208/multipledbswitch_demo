﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Practise.core.Db;
using Practise.core.Common;
using Dapper;
using Practise.core.Patient;

namespace Practise.Infrastructure
{
    public class DbRepositiry : IDbRepositiry
    {
        public readonly IConfiguration _configuration;

        private static string? connectionString = string.Empty;
        private readonly DapperHelperRepo _dapperHelperRepo;


        public DbRepositiry(IConfiguration configuration, DapperHelperRepo dapperHelperRepo)
        {
            _configuration = configuration;
            connectionString = _configuration["ConnectionStrings:PracticeDemo"]!;
            _dapperHelperRepo = dapperHelperRepo;
        }

        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(connectionString);
            }
        }

        //public async Task<Response<DbGetResponse>> DbGetAsync(DbGetRequest dbGetRequestId)
        //{
        //    Response<DbGetResponse> response;
        //    using IDbConnection db = Connection;
        //    var result = await db.QueryMultipleAsync("[dbo].[DbNameGet]", dbGetRequestId);
        //    response = result.Read<Response<DbGetResponse>>().FirstOrDefault()!;
        //    response.Data = response.Status ? result.Read<DbGetResponse>().ToList() : default;
        //    return response;
        //}

        public async Task<ResponseNew<DbGetResponse>> DbGetAsync(DbGetRequest dbGetRequestId)
        {
            ResponseNew<DbGetResponse> response;
            using IDbConnection db = Connection;
            var result = await db.QueryMultipleAsync("[dbo].[UserGet]", dbGetRequestId);
            response = result.Read<ResponseNew<DbGetResponse>>().FirstOrDefault()!;
            response.Data = response.Status ? result.Read<DbGetResponse>().FirstOrDefault() : null;
            return response;
        }

        //public async Task<List<UserGetResponse>> UserGetAsync(UserGetRequest userGetRequest)
        //{
        //    //Response<UserGetResponse> response;
        //    //using IDbConnection db = Connection;
        //    //var result = await db.QueryMultipleAsync("[dbo].[UserGet]", userGetRequest);
        //    //response = result.Read<Response<UserGetResponse>>().FirstOrDefault()!;
        //    //response.Data = response.Status ? result.Read<UserGetResponse>().ToList() : default;
        //    //return response;
        //}
        public async Task<List<UserGetResponse>> UserGetAsync(UserGetRequest userGetRequest)
        {
            List<UserGetResponse> response;
            var result = _dapperHelperRepo.QueryMultiple<UserGetResponse>("[dbo].[UserGet]", new { userGetRequest.Id });
            return result;
        }

        public async Task<List<DbGetDllResponse>> DbDDLgetlist(DbGetDllRequest dbGetDll)
        {
            List<DbGetDllResponse> dbGetDllResponses;
            var result = _dapperHelperRepo.QueryMultiple<DbGetDllResponse>("[dbo].[GetDBDll]", dbGetDll);
            return result;
        }
    }
}
    //    private readonly IConfiguration _configuration;

//    public DbRepositiry(IConfiguration configuration)
//    {
//        _configuration = configuration;
//    }

//    private IDbConnection GetConnection(string connectionString)
//    {
//        return new SqlConnection(connectionString);
//    }

//    public async Task<ResponseNew<DbGetResponse>> DbGetAsync(DbGetRequest dbGetRequestId)
//    {
//        ResponseNew<DbGetResponse> response;
//        string connectionString = _configuration.GetConnectionString("PracticeDemo");
//        using IDbConnection db = GetConnection(connectionString);
//        var result = await db.QueryMultipleAsync("[dbo].[DbNameGet]", dbGetRequestId);
//        response = result.Read<ResponseNew<DbGetResponse>>().FirstOrDefault()!;
//        response.Data = response.Status ? result.Read<DbGetResponse>().FirstOrDefault() : null;
//        return response;
//    }

//    public async Task<Response<UserGetResponse>> UserGetAsync(UserGetRequest userGetRequest, string dbName)
//    {
//        Response<UserGetResponse> response;
//        string connectionString = string.Format(_configuration.GetConnectionString("Test"), dbName);
//        using IDbConnection db = GetConnection(connectionString);
//        var result = await db.QueryMultipleAsync("[dbo].[UserGet]", userGetRequest);
//        response = result.Read<Response<UserGetResponse>>().FirstOrDefault()!;
//        response.Data = response.Status ? result.Read<UserGetResponse>().FirstOrDefault() : null;
//        return response;
//    }


//}
//}
