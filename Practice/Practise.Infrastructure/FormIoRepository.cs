﻿using Microsoft.Extensions.Configuration;
using Practise.core.Common;
using Practise.core.FormIo;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Practise.core.Patient;
using System.Data.Common;

namespace Practise.Infrastructure
{
    public class FormIoRepository : IFormIoRepository
    {

        private readonly IConfiguration _configuration;

        private static string con = string.Empty;
        public FormIoRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            con = _configuration["ConnectionStrings:PracticeDemo"]!;

        }
        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(con);
            }
        }

        public async Task<Response> FormAdd(DtoForm dtoForm)
        {
            Response? response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[dbo].[uspAddForm]", dtoForm);
            return response!;
        }


        public async Task<ResponseData<Formresponse>> FormsGetListAsync()
        {
            ResponseData<Formresponse> response;
            using IDbConnection db = Connection;
            var result = await db.QueryMultipleAsync("[dbo].[GetFormDll]", new {});
            response = result.Read<ResponseData<Formresponse>>().FirstOrDefault()!;
            response.Data = response.Status ? result.Read<Formresponse>().ToList() : default;
            return response;
        }



        //public async Task<ResponseData<DtoForm>> FormsGetListAsync()
        //{
        //    ResponseData<DtoForm> response;
        //    using IDbConnection db = Connection;
        //    var result = await db.QueryMultipleAsync("[dbo].[uspGetPatientList]");
        //    response = result.Read<ResponseData<DtoForm>>().FirstOrDefault()!;
        //    response.Data = response.Status ? result.Read<DtoForm>().ToList() : default(List<DtoForm>);

        //   // response.Data = response.Status ? result.Read<DtoForm>().ToList() : default;
        //    return response;
        //}
    }
}
