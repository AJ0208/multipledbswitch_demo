﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practise.core.FormIo
{
    public  class DtoForm
    {
        public string? FormName { get; set; }
        public string? CreatedForm {  get; set; }
    }

    public class Formresponse
    {
        public string? FormName { get; set; }
        public string? CreatedForm { get; set; }
    }
    public class Formrequest
    {
        public string? Type { get; set; }
    }

  
}
