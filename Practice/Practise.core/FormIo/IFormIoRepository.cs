﻿using Practise.core.Common;
using Practise.core.Patient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practise.core.FormIo
{
    public interface IFormIoRepository
    {
        Task<Response> FormAdd(DtoForm dtoForm);
        Task<ResponseData<Formresponse>> FormsGetListAsync();
        //Task<List<Formresponse>> FormsGetListAsync(Formrequest formrequest);
    }
}
