﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practise.core.Patient
{
    public class Patient
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Gender { get; set; }
        public string? DOB { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Address { get; set; }
    }

    public class  PatienSave : Patient
    {
       public string? CreatedBy { get; set; }
    }

    public class PatientUpdate : Patient 
    {
        public string? PatientId { get; set; }
        public string? UpdatedBy { get; set; }
    }

    public class PatientDelete
    {
        public string? PatientId { get; set; }
        public string? DeletedBy { get; set; }
    }

    public class PatientGetById
    {
        public string? PatientId { get;  set; }
    }

    public class PatientGetList
    {
        public string? SearchKey { get; set; } 
        public int? Start { get; set; }
        public int? PageSize { get; set; }
        public string? SortCol { get; set; } 
    }

        
    
}
