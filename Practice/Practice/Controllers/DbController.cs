﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Practise.core.Db;
using Practise.core.Patient;
using Practise.Infrastructure;

namespace Practice.Controllers
{
    [Route("api/Db")]
    //[ApiController]
    public class DbController : ControllerBase
    {
        private readonly IDbRepositiry _dbRepositiry;
        private readonly DapperHelperRepo _dapperHelperRepo;

        public DbController(IDbRepositiry dbRepositiry , DapperHelperRepo dapperHelperRepo )
        {
            _dbRepositiry = dbRepositiry;
            _dapperHelperRepo = dapperHelperRepo;
        }

        private async Task<ActionResult<T>> HandleInvalidModelState<T>(Func<Task<T>> action)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    Status = false,
                    Message = string.Join(Environment.NewLine, ModelState.Values
                                            .SelectMany(x => x.Errors)
                                            .Select(x => x.ErrorMessage)),

                });
            }

            T response = await action();

            if (response == null)
            {
                return NotFound();
            }

            return Ok(response);
        }

        private async Task<ActionResult<T>> HandleException<T>(Func<Task<T>> action)
        {
            try
            {
                return await HandleInvalidModelState(action);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost, Route("dbGet")]
        public async Task<IActionResult> DbGetAsync([FromBody] DbGetRequest dbGetRequestId)
        {
            try
            {

                var response = await _dbRepositiry.DbGetAsync(dbGetRequestId);
                return Ok(response);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpPost, Route("userGet")]
        public async Task<IActionResult> UserGetAsync([FromBody] UserGetRequest userGetRequest)
        {
            try
            {
                var DbId = HttpContext.Request.Headers["Id"].FirstOrDefault();
                _dapperHelperRepo.DbResponse = _dapperHelperRepo.GetDbName(int.Parse(DbId!));
                _dapperHelperRepo.IsMasterDb = false;
                var response = await _dbRepositiry.UserGetAsync(userGetRequest);
                return Ok(response);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpGet, Route("DbDDLgetlist")]
        public async Task<IActionResult> DbDLLList([FromQuery] DbGetDllRequest dbGetDll)
        {
            try
            {
                var response = await _dbRepositiry.DbDDLgetlist(dbGetDll);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }




        //[HttpPost, Route("userGet")]
        //public async Task<IActionResult> CallApisAsync([FromBody]DbGetRequest dbGetRequest, UserGetRequest userGetRequest)
        //{
        //    try
        //    {
        //        // Call the DbGetAsync endpoint to get the database name
        //        var dbResponse = await _dbRepositiry.DbGetAsync(dbGetRequest);

        //        // Check if the response from DbGetAsync is successful
        //        if (dbResponse.Status)
        //        {
        //            // Extract the database name from the response
        //            var dbName = dbResponse.Data?.DbName;

        //            if (!string.IsNullOrEmpty(dbName))
        //            {
        //                // Use the retrieved database name to call the UserGetAsync endpoint
        //                var userResponse = await _dbRepositiry.UserGetAsync(userGetRequest, dbName);

        //                // Check if the response from UserGetAsync is successful
        //                if (userResponse.Status)
        //                {
        //                    // Return the response from UserGetAsync
        //                    return Ok(userResponse.Data); // Assuming the content of the response is what you want to return
        //                }
        //                else
        //                {
        //                    // Return the status code and message from UserGetAsync
        //                    return StatusCode(StatusCodes.Status500InternalServerError, $"UserGetAsync failed: {userResponse.Message}");
        //                }
        //            }
        //            else
        //            {
        //                // Database name is null or empty
        //                return StatusCode(StatusCodes.Status500InternalServerError, "Database name is null or empty.");
        //            }
        //        }
        //        else
        //        {
        //            // Return the status code and message from DbGetAsync
        //            return StatusCode(StatusCodes.Status500InternalServerError, "DbGetAsync failed: " + dbResponse.Message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // Return 500 Internal Server Error with the exception message
        //        return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        //    }
        //}



    }
}
