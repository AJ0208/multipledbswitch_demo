﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Practise.core.FormIo;
using Practise.core.Patient;
using Practise.Infrastructure;

namespace Practice.Controllers
{
    [Route("api/FormIo")]
    //[ApiController]
    public class FormIoController : ControllerBase
    {
        private readonly IFormIoRepository _formIoRepository;
        public FormIoController(IFormIoRepository formIoRepository)
        { 
            _formIoRepository = formIoRepository;
        }


        private async Task<ActionResult<T>> HandleInvalidModelState<T>(Func<Task<T>> action)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    Status = false,
                    Message = string.Join(Environment.NewLine, ModelState.Values
                                            .SelectMany(x => x.Errors)
                                            .Select(x => x.ErrorMessage)),

                });
            }

            T response = await action();

            if (response == null)
            {
                return NotFound();
            }

            return Ok(response);
        }


        private async Task<ActionResult<T>> HandleException<T>(Func<Task<T>> action)
        {
            try
            {
                return await HandleInvalidModelState(action);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


       
        [HttpPost, Route("addForm")]
        public async Task<IActionResult> FormSaveAsync([FromBody] DtoForm dtoForm)
        {
            try
            {
                var response = await _formIoRepository.FormAdd(dtoForm);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpGet, Route("formdetails")]
        public async Task<IActionResult> FormGetListAsync()
        {
            try
            {
                var response = await _formIoRepository.FormsGetListAsync();
                return Ok(response);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }
    }
}
